#!/usr/bin/env python

import os

steamfolder = "/media/juney/drawers/juney/Steam/"
modsfolder = steamfolder + "userdata/52361248/ugc/referenced/"

def search(file_path):
    # Searches the list of mods for any files ending in ".civ5mod"
    for root, dirs, filenames in os.walk(file_path):
        for item in filenames:
            if item.endswith(".civ5mod"):
                return (item, root)

def rename_dirs(modname, root):
    # Renames the parent directory after the mod file
    os.rename(root, os.path.dirname(root) + "/" + item.split("."[0]))

if __name__ == '__main__':
    modlist = []
    
    # Make a list of directories in the Mods directory.
    for root, dirs, files in os.walk(modsfolder):
        modlist.extend(dirs)
    
    # Rename each dir to the name of the .civ5mod file inside it.
    for item in modlist:
        file_path = modsfolder + item
        search_results = search(file_path)
        
        if search_results:
            rename_dir(search_results)
