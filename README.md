This is a python script for renaming the Civ V mods folders on linux for easier installation.

When called, the script will search downloaded mods folders (named with a reference number), identify any .civ5mod files (archives), then rename the parent folder after the identified filename.

The script is only written to run to local directories on one machine at the moment--change the steamfolder string to your Steam installation directory, and the modsfolder string to your corresponding mods folder.

Next commits should be adding formatting abd modularizing, PEP 8 compliancy, then rewriting the Steam directory code to be compatible with any directory.
